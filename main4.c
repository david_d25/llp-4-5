#include <stdio.h>
#include <stdlib.h>

#include "linked_list.h"
#include "higher_order_functions.h"

void printNumber(int i) {
    printf("%d ", i);
}

int main() {
    list_node* list = NULL;
    printf("Labwork 4\n");
    printf("Write your numbers right here -> ");
    while (1) {
        int input;
        char afterInput;
        int result = scanf("%d%c", &input, &afterInput);
        if (result == EOF || result == 0)
            break;
        list_add_front(&list, input);
        if (afterInput == '\n')
            break;
    }
    printf("Given array: ");
    list_foreach(&list, printNumber);
    printf("\n");
    printf("Sum: %d\n", list_sum(&list));
    list_free(&list);
    printf("The list has been freed\n");
    return 0;
}