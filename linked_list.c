#include <malloc.h>
#include <stdio.h>
#include <stdbool.h>

#include "linked_list.h"

list_node* list_create(int first_elem_value) {
    list_node* result = (list_node*)malloc(sizeof(list_node));
    result->value = first_elem_value;
    return result;
}

void list_add_front(list_node** list, int value) {
    list_node* new_node = list_create(value);
    new_node->next = *list;
    *list = new_node;
}

void list_add_back(list_node** list, int value) {
    list_node* last = *list;
    list_node* new_node = list_create(value);
    if (last != NULL) {
        while (last->next != NULL)
            last = last->next;
        last->next = new_node;
    } else {
        *list = new_node;
    }
}

int list_get(list_node** list, size_t index) {
    list_node* result = list_node_at(list, index);
    if (result != NULL)
        return result->value;
    return 0;
}

list_node* list_free(list_node** list) {
    list_node* current = *list;
    while (current != NULL) {
        list_node* next = current->next;
        free(current);
        current = next;
    }
    *list = NULL;
}

size_t list_length(list_node** list) {
    size_t result = 0;
    list_node* current = *list;
    while (current != NULL) {
        result++;
        current = current->next;
    }
    return result;
}

list_node* list_node_at(list_node** list, size_t index) {
    if (index < 0)
        return NULL;

    list_node* current = *list;
    for (int i = 0; i < index; i++) {
        if (current->next != NULL)
            current = current->next;
        else
            return NULL;
    }
    return current;
}

int list_sum(list_node** list) {
    list_node* current = *list;
    int result = 0;
    while (current != NULL) {
        result += current->value;
        current = current->next;
    }
    return result;
}

bool save(list_node** list, const char* file_name) {
    FILE* file = fopen(file_name, "w");
    list_node* current = *list;
    while (current != NULL) {
        int rv = fprintf(file, "%d ", current->value);
        if (rv < 1) {
            fclose(file);
            return false;
        }
        current = current->next;
    }
    fclose(file);
    return true;
}

bool load(list_node** list, const char* file_name) {
    FILE* file = fopen(file_name, "r");
    if (file == NULL)
        return false;

    while (1) {
        int val;
        int rv = fscanf(file, "%d", &val);
        if (ferror(file)) {
            return false;
            fclose(file);
        }
        if (feof(file))
            break;
        list_add_back(list, val);
    }
    fclose(file);
    return true;
}

bool serialize(list_node** list, const char* file_name) {
    FILE* file = fopen(file_name, "wb");
    list_node* current = *list;
    while (current != NULL) {
        int rv = fwrite(&current->value, sizeof(current->value), 1, file);
        if (rv < 1) {
            fclose(file);
            return false;
        }
        current = current->next;
    }
    fclose(file);
    return true;
}

bool deserialize(list_node** list, const char* file_name) {
    FILE* file = fopen(file_name, "rb");
    if (file == NULL)
        return false;

    while (1) {
        int val;
        int rv = fread(&val, sizeof(val), 1, file);
        if (ferror(file)) {
            return false;
            fclose(file);
        }
        if (feof(file))
            break;
        list_add_back(list, val);
    }
    fclose(file);
    return true;
}