#ifndef HIGHER_ORDER_FUNCTIONS
#define HIGHER_ORDER_FUNCTIONS

#include <stdlib.h>

#include "linked_list.h"

int list_foreach(list_node**, void (*)(int));
list_node* list_map(list_node**, int (*)(int));
void list_map_mut(list_node**, int (*)(int));
int foldl(list_node**, int (*)(int, int), int);
list_node* iterate(int, size_t, int (*)(int));

#endif