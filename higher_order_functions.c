#include "higher_order_functions.h"

int list_foreach(list_node** list, void (*func)(int)) {
    list_node* current = *list;
    while (current != NULL) {
        func(current->value);
        current = current->next;
    }
}

list_node* list_map(list_node** src, int (*mapper)(int)) {
    list_node* current = *src;
    list_node* result = NULL;
    while (current != NULL) {
        list_add_back(&result, mapper(current->value));
        current = current->next;
    }
    return result;
}

void list_map_mut(list_node** mutable_list, int (*mapper)(int)) {
    list_node* current = *mutable_list;
    while (current != NULL) {
        current->value = mapper(current->value);
        current = current->next;
    }
}

int foldl(list_node** list, int (*folder)(int, int), int accumulator) {
    list_node* current = *list;
    while (current != NULL) {
        accumulator = folder(current->value, accumulator);
        current = current->next;
    }
    return accumulator;
}

list_node* iterate(int initial, size_t length, int (*iterator)(int)) {
    list_node* result = NULL;
    int current = initial;
    for (size_t i = 1; i < length; i++) {
        list_add_back(&result, current);
        current = iterator(current);
    }
    return result;
}