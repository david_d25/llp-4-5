all: main4 main5

main4: main4.c linked_list.c higher_order_functions.c
	gcc -o lab4 main4.c linked_list.c higher_order_functions.c

main5: main4.c linked_list.c higher_order_functions.c
	gcc -o lab5 main5.c linked_list.c higher_order_functions.c