#ifndef LINKED_LIST
#define LINKED_LIST

#include <stdbool.h>

struct list_node {
    int value;
    struct list_node* next;
};

typedef struct list_node list_node;

int list_get(list_node**, size_t);
int list_sum(list_node**);
bool save(list_node**, const char*);
bool load(list_node**, const char*);
bool serialize(list_node**, const char*);
bool deserialize(list_node**, const char*);
void list_add_back(list_node**, int);
void list_add_front(list_node**, int);
size_t list_length(list_node**);
list_node* list_free(list_node**);
list_node* list_create(int);
list_node* list_node_at(list_node**, size_t);

#endif