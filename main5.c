#include <stdio.h>
#include <stdlib.h>

#include "linked_list.h"
#include "higher_order_functions.h"

void print_with_space(int i) {
    printf("%d ", i);
}

void print_with_newline(int i) {
    printf("%d\n", i);
}

int square(int i) {
    return i*i;
}

int cube(int i) {
    return i*i*i;
}

int min(int a, int b) {
    return a < b ? a : b;
}

int max(int a, int b) {
    return a > b ? a : b;
}

int module(int a) {
    return a >= 0 ? a : -a;
}

int mul2(int a) {
    return a*2;
}

int main() {
    list_node* list = NULL;
    printf("Labwork 5\n");
    printf("Write your numbers right here -> ");
    while (1) {
        int input;
        char afterInput;
        int result = scanf("%d%c", &input, &afterInput);
        if (result == EOF || result == 0)
            break;
        list_add_back(&list, input);
        if (afterInput == '\n')
            break;
    }
    printf("\n");
    printf("Foreach output 1: ");
    list_foreach(&list, print_with_space);
    printf("\n");
    
    printf("Foreach output 2:\n");
    list_foreach(&list, print_with_newline);
    
    printf("Map squares: ");
    list_node* map_squares_list = list_map(&list, square);
    list_foreach(&map_squares_list, print_with_space);
    printf("\n");
    list_free(&map_squares_list);

    printf("Map cubes: ");
    list_node* map_cubes_list = list_map(&list, cube);
    list_foreach(&map_cubes_list, print_with_space);
    printf("\n");
    list_free(&map_cubes_list);

    if (list_length(&list) > 0) {
        int minVal = foldl(&list, min, list->value);
        printf("Min: %d\n", minVal);
        int maxVal = foldl(&list, max, list->value);
        printf("Max: %d\n", maxVal);
    } else {
        printf("The list is empty, can't compute min and max\n");
    }

    list_map_mut(&list, module);
    printf("Map mut module: ");
    list_foreach(&list, print_with_space);
    printf("\n");

    list_node* powers_list = iterate(1, 10, mul2);
    printf("Iterate pow2: ");
    list_foreach(&powers_list, print_with_space);
    printf("\n");
    list_free(&powers_list);

    bool saveOk = save(&list, "list.txt");
    if (saveOk) {
        printf("List saved\n");
        list_free(&list);
        bool loadOk = load(&list, "list.txt");

        if (loadOk) {
            printf("Loaded list: ");
            list_foreach(&list, print_with_space);
            printf("\n");
        } else
            printf("List not loaded D:\n");
    } else {
        printf("List not saved D:\n");
    }

    bool serializeOk = serialize(&list, "list_bin");
    if (serializeOk) {
        printf("List serialized\n");
        list_free(&list);
        bool deserializeOk = deserialize(&list, "list_bin");

        if (serializeOk) {
            printf("Deserialized list: ");
            list_foreach(&list, print_with_space);
            printf("\n");
        } else
            printf("List not deserialized D:\n");
    } else {
        printf("List not serialized D:\n");
    }

    list_free(&list);
    printf("The list has been freed\n");
    return 0;
}